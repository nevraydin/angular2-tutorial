import { RxApiExamplesPage } from './app.po';

describe('rx-api-examples App', () => {
  let page: RxApiExamplesPage;

  beforeEach(() => {
    page = new RxApiExamplesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
