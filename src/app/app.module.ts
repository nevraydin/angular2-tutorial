import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule,XHRBackend   } from '@angular/http';
import { RouterModule }   from '@angular/router';
//import {EventItemComponent } from './api/event-item.component'
//import {ApiService} from './api/api.service.component'

import { InMemoryWebApiModule  } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppComponent } from './app.component';
import { HeroDetailComponent } from "app/hero-detail.component";
import { HeroService } from "app/hero.service";
import { HeroesComponent } from "app/heroes.component";
import { DashboardComponent } from "app/dashboard.component";
import { HeroSearchComponent } from "app/hero-search.component";
//import { AppRoutingModule } from "app/app-routing.module";


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    RouterModule.forRoot([
  {
    path: 'heroes',
    component: HeroesComponent
  },
  {
  path: 'dashboard',
  component: DashboardComponent,
  pathMatch:'full'
},
{
  path: 'detail/:id',
  component: HeroDetailComponent
},

])
  ],
  declarations: [
    AppComponent,
    HeroDetailComponent,
    HeroesComponent,
    DashboardComponent,
    HeroSearchComponent
  ],
  providers: [
    HeroService
  ],
  bootstrap: [ AppComponent]
})
export class AppModule {
}
